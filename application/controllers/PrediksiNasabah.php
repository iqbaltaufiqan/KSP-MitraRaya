<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PrediksiNasabah extends CI_Controller {

    function __construct()
	{
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            redirect(base_url('auth'));
        }
        $this->load->model('M_Datatrain');
        $this->load->model('M_Datatest');
        $this->load->model('M_Pengajuan');
	}

	public function index()
	{
        $this->load->view('prediksi_nasabah/submit_data.php');
    }
    
    public function submit_data()
    {
        
    }

    public function proses_submit()
    {
        $nama = $this->input->post('nama');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $total_pinjaman = $this->input->post('total_pinjaman');
        $jumlah_tanggungan = $this->input->post('jumlah_tanggungan');
        $status_perkawinan = $this->input->post('status_perkawinan');
        $pendapatan_utama = $this->input->post('pendapatan_utama');
        $total_biaya_hidup = $this->input->post('total_biaya_hidup');
        $status_pinjaman = "MACET";
        $ttotal_pinjaman = NULL;
        $tjumlah_tanggungan = NULL;
        $tpendapatan_utama = NULL;
        $ttotal_biaya_hidup = NULL;

        // transformation data total pinjaman
        if ($total_pinjaman >= 0 && $total_pinjaman <= 2500000) {
            $ttotal_pinjaman = "SANGAT_RENDAH";
        }
        elseif ($total_pinjaman >= 2500001 && $total_pinjaman <= 4500000) {
            $ttotal_pinjaman = "RENDAH";
        }
        elseif ($total_pinjaman >= 4500001 && $total_pinjaman <= 6000000) {
            $ttotal_pinjaman = "SEDANG";
        }
        elseif ($total_pinjaman >= 6000001 && $total_pinjaman <= 8000000) {
            $ttotal_pinjaman = "TINGGI";
        }
        else{
            $ttotal_pinjaman = "SANGAT_TINGGI";
        }

        // transformation data jumlah tanggungan
        if ($jumlah_tanggungan >= 0 && $jumlah_tanggungan <= 2) {
            $tjumlah_tanggungan = "SEDIKIT";
        }
        elseif ($jumlah_tanggungan >= 3 && $jumlah_tanggungan <= 4) {
            $tjumlah_tanggungan = "SEDANG";
        }
        else{
            $tjumlah_tanggungan = "BANYAK";
        }

        // transformation data pendapatan utama
		if ($pendapatan_utama >= 0 && $pendapatan_utama <= 1500000) {
			$tpendapatan_utama = "RENDAH";
		}
		elseif ($pendapatan_utama >= 1500001 && $pendapatan_utama <= 3000000) {
			$tpendapatan_utama = "SEDANG";
		}
		else{
			$tpendapatan_utama = "TINGGI";
        }
        
        // transformation data total biaya hidup
		if ($total_biaya_hidup >= 0 && $total_biaya_hidup <= 1500000) {
			$ttotal_biaya_hidup = "RENDAH";
		}
		elseif ($total_biaya_hidup >= 1500001 && $total_biaya_hidup <= 3000000) {
			$ttotal_biaya_hidup = "SEDANG";
		}
		else{
			$ttotal_biaya_hidup = "TINGGI";
        }
        
        $id = NULL;
        $id_pengajuan = NULL;
        $last_records = $this->M_Datatest->get_last_record();
        foreach ($last_records as $key => $value) {
            $id = $value->id+1;
        }

        $last_record_pengajuan = $this->M_Pengajuan->get_data();
        if (count($last_record_pengajuan) == 0) {
            $id_pengajuan = 1;
        }
        else{
            foreach ($last_record_pengajuan as $key => $last) {
                $id_pengajuan = $last->id + 1;
            }
        }

        $date = date('Y-m-d');

        $data = array('id' => $id, 'jenis_kelamin' => $jenis_kelamin, 'total_pinjaman' => $ttotal_pinjaman, 'jumlah_tanggungan' => $tjumlah_tanggungan, 'status_pernikahan' => $status_perkawinan, 'pendapatan_utama' => $tpendapatan_utama, 'total_biaya_hidup' => $ttotal_biaya_hidup, 'status_pinjaman' => $status_pinjaman);
        $data_pengajuan = array('id' => $id_pengajuan, 'nama' => $nama, 'jenis_kelamin' => $jenis_kelamin, 'total_pinjaman' => $total_pinjaman, 'jumlah_tanggungan' => $jumlah_tanggungan, 'status_pernikahan' => $status_perkawinan, 'pendapatan_utama' => $pendapatan_utama, 'total_biaya_hidup' => $total_biaya_hidup, 'tgl_pengajuan' => $date);
        
        $this->M_Datatest->input($data);
        $this->M_Pengajuan->input($data_pengajuan);
        $this->export_csv();
        $this->convert_datatest_to_arff();
        $this->make_predict($id_pengajuan);

        $data['pengajuan'] = $this->M_Pengajuan->get_last_record();
        $this->load->view('prediksi_nasabah/hasil_prediksi.php', $data);
    }

    public function export_csv()
    {
        if (file_exists("./assets/uploads/datatest.csv")) {
			unlink("./assets/uploads/datatest.csv");
		}
        $filename = 'datatest.csv';
        $records = $this->M_Datatest->get_data_desc();
        $data = "JENIS_KELAMIN,TOTAL_PINJAMAN,JUMLAH_TANGGUNGAN,STATUS_PERNIKAHAN,PENDAPATAN_UTAMA,TOTAL_BIAYA_HIDUP,STATUS_PINJAMAN \n";//Column headers
        foreach ($records as $key => $value) {
            $data.= $value->jenis_kelamin.','.$value->total_pinjaman.','.$value->jumlah_tanggungan.','.$value->status_pernikahan.','.$value->pendapatan_utama.','.$value->total_biaya_hidup.','.$value->status_pinjaman."\n"; //Append data to csv
        }
		file_put_contents('./assets/uploads/' . $filename, $data);
    }

    public function convert_datatest_to_arff()
    {
		if (file_exists("./assets/uploads/datatest.arff")) {
			unlink("./assets/uploads/datatest.arff");
		}
		$cmd = "java -cp C:\/xampp\htdocs\KSP-MitraRaya\assets\weka.jar weka.core.converters.CSVLoader C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatest.csv > C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatest.arff";
		exec($cmd, $output);
    }

    public function make_predict($id_pengajuan)
    {
        $cmd = "java -cp C:\/xampp\htdocs\KSP-MitraRaya\assets\weka.jar weka.classifiers.misc.InputMappedClassifier -I -trim -L C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.model -t C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.arff -T C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatest.arff -p 0";
        exec($cmd, $output);
        $explode = explode(":", $output[17]);
        $explode2 = explode(" ", $explode[2]);
        $result = $explode2[0];
        // echo $result;
        $data_array = array('status_pinjaman' => $result);
        $cond = array('id' => $id_pengajuan);
        $this->M_Pengajuan->update($data_array, $cond);
    }

    public function lihat_akurasi()
    {
        $cmd = shell_exec("java -cp C:\/xampp\htdocs\KSP-MitraRaya\assets\weka.jar weka.classifiers.trees.J48 -t C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.arff -v -o");
        $data['row'] = $cmd;
        $this->load->view('prediksi_nasabah/lihat_akurasi.php', $data);
    }

    public function lihat_tree()
    {
        $cmd = shell_exec("java -cp C:\/xampp\htdocs\KSP-MitraRaya\assets\weka.jar weka.classifiers.trees.J48 -t C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.arff -v -no-cv");
        $data['row'] = $cmd;

        $this->load->view('prediksi_nasabah/lihat_tree.php', $data);
    }
}
