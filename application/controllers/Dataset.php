<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url('auth'));
		}
		$this->load->model('M_Nasabah');
		$this->load->model('M_Transaksi');
		$this->load->model('M_Datatrain');
		$this->load->model('M_Datatest');
		$this->load->library('csvimport');
	}

	public function index()
	{
		$this->load->view('Dataset/form_dataset.php');
	}

	public function admin(Type $var = null)
	{
		$this->load->view('Dataset/form_dataset_admin.php');
	}

	public function proses_upload()
	{	
		if (file_exists("./assets/uploads/upload_datatrain.csv")) {
			unlink("./assets/uploads/upload_datatrain.csv");
		}

		$name_file = "upload_datatrain";
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'csv';
		$config['max_size'] = '2048';
		$config['file_name'] = $name_file;

		$this->load->library('upload', $config);

		$this->upload->initialize($config);
		if ($this->upload->do_upload('dataset')) {
			$file_upload = $this->upload->data();
			$file_path = './assets/uploads/'.$file_upload['file_name'];

			if ($this->csvimport->get_array($file_path)) {
				$csv_array = $this->csvimport->get_array($file_path);
				$count_row = $this->M_Nasabah->get_row();
				if ($count_row > 0) {
					$this->M_Nasabah->delete_all_data();
					$this->M_Transaksi->delete_all_data();
				}
				foreach ($csv_array as $key => $value) {
					$data_nasabah[] = array('id' => $value['No'], 'nama' => $value['Nama_Anggota'], 'jenis_kelamin' => $value['Jenis_Kelamin'], 'alamat' => $value['Alamat'], 'pekerjaan' => $value['Pekerjaan'], 'jumlah_tanggungan' => $value['Jumlah_Tanggungan'], 'status_pernikahan' => $value['Status_Pernikahan']);
					$data_transaksi[] = array('id' => $value['No'], 'id_nasabah' => $value['No'], 'total_pinjaman' => $value['Total_Pinjaman'], 'agunan' => $value['Tipe_Agunan'], 'pendapatan_utama' => $value['Pendapatan_Utama'], 'total_biaya_hidup' => $value['Total_Biaya_Hidup'], 'status_pinjaman' => $value['Status_Pinjaman'] );
				}
				$this->M_Nasabah->input($data_nasabah);
				$this->M_Transaksi->input($data_transaksi);
			}
			$this->export_csv();
			$this->get_data_from_data_train();
			$this->convert_datatrain_to_arff();
			$this->create_model();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" style=\"pointer:cursor;position: fixed; z-index: 9999;top: 0px;margin-top: 50px; margin-left: 10px; width:98.5%;\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a> Penambahan Dataset Berhasil!!! </div>");
			redirect(base_url('dataset'));
		}
		else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" style=\"pointer:cursor;position: fixed; z-index: 9999;top: 0px;margin-top: 50px; margin-left: 10px; width:98.5%;\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a> Penambahan Dataset Gaga!!! </div>");
			redirect(base_url('dataset'));
		}
	}

	public function export_csv()
	{
		$filename = 'datatrain.csv';
		$records = $this->M_Nasabah->get_records_to_create_csv();

		$data = "JENIS_KELAMIN,TOTAL_PINJAMAN,JUMLAH_TANGGUNGAN,STATUS_PERNIKAHAN,PENDAPATAN_UTAMA,TOTAL_BIAYA_HIDUP,STATUS_PINJAMAN \n";//Column headers
		foreach ($records as $key => $value) {
			$jenis_kelamin = NULL;
			$total_pinjaman = NULL;
			$jumlah_tanggungan = NULL;
			$pendapatan_utama = NULL;
			$total_biaya_hidup = NULL;
			$status_pernikahan = NULL;
			$id = $key + 1;

			// transformation data jenis kelamin
			if ($value->jenis_kelamin == "LAKI-LAKI") {
				$jenis_kelamin = "L";
			}
			elseif ($value->jenis_kelamin == "PEREMPUAN") {
				$jenis_kelamin = "P";
			}

			// transformation data jumlah tanggungan
			if ($value->jumlah_tanggungan >= 0 && $value->jumlah_tanggungan <= 2) {
				$jumlah_tanggungan = "SEDIKIT";
			}
			elseif ($value->jumlah_tanggungan >= 3 && $value->jumlah_tanggungan <= 4) {
				$jumlah_tanggungan = "SEDANG";
			}
			else{
				$jumlah_tanggungan = "BANYAK";
			}

			// transformation data status pernikahan
			if ($value->status_pernikahan == "MENIKAH") {
				$status_pernikahan = "MENIKAH";
			}
			elseif ($value->status_pernikahan == "BELUM MENIKAH") {
				$status_pernikahan = "BELUM_MENIKAH";
			}
			elseif ($value->status_pernikahan == "CERAI") {
				$status_pernikahan = "CERAI";
			}

			// transformation data total pinjaman
			if ($value->total_pinjaman >= 0 && $value->total_pinjaman <= 2500000) {
				$total_pinjaman = "SANGAT_RENDAH";
			}
			elseif ($value->total_pinjaman >= 2500001 && $value->total_pinjaman <= 4500000) {
				$total_pinjaman = "RENDAH";
			}
			elseif ($value->total_pinjaman >= 4500001 && $value->total_pinjaman <= 6000000) {
				$total_pinjaman = "SEDANG";
			}
			elseif ($value->total_pinjaman >= 6000001 && $value->total_pinjaman <= 8000000) {
				$total_pinjaman = "TINGGI";
			}
			else{
				$total_pinjaman = "SANGAT_TINGGI";
			}

			// transformation data pendapatan utama
			if ($value->pendapatan_utama >= 0 && $value->pendapatan_utama <= 1500000) {
				$pendapatan_utama = "RENDAH";
			}
			elseif ($value->pendapatan_utama >= 1500001 && $value->pendapatan_utama <= 3000000) {
				$pendapatan_utama = "SEDANG";
			}
			else{
				$pendapatan_utama = "TINGGI";
			}

			// transformation data total biaya hidup
			if ($value->total_biaya_hidup >= 0 && $value->total_biaya_hidup <= 1500000) {
				$total_biaya_hidup = "RENDAH";
			}
			elseif ($value->total_biaya_hidup >= 1500001 && $value->total_biaya_hidup <= 3000000) {
				$total_biaya_hidup = "SEDANG";
			}
			else{
				$total_biaya_hidup = "TINGGI";
			}

			$data.= $jenis_kelamin.','.$total_pinjaman.','.$jumlah_tanggungan.','.$status_pernikahan.','.$pendapatan_utama.','.$total_biaya_hidup.','.$value->status_pinjaman."\n"; //Append data to csv
			$data_train[] = array('id' => $id, 'jenis_kelamin' => $jenis_kelamin, 'total_pinjaman' => $total_pinjaman, 'jumlah_tanggungan' => $jumlah_tanggungan, 'status_pernikahan' => $status_pernikahan, 'pendapatan_utama' => $pendapatan_utama, 'total_biaya_hidup' => $total_biaya_hidup, 'status_pinjaman' => $value->status_pinjaman );
		}
		$check_num = $this->M_Datatrain->get_num_rows_data();
		if ($check_num > 0) {
			$this->M_Datatrain->delete_all_data();
		}
		$this->M_Datatrain->input($data_train);
		if (file_exists("./assets/uploads/datatrain.csv")) {
			unlink("./assets/uploads/datatrain.csv");
		}
		file_put_contents('./assets/uploads/' . $filename, $data);
	}

	public function get_data_from_data_train()
    {
        $check_num = $this->M_Datatest->get_num_rows_data();
        if ($check_num > 0) {
            $this->M_Datatest->delete_all_data();
        }

        $data_jenis_kelamin = $this->M_Datatrain->get_jenis_kelamin();
        $count_jenis_kelamin = count($data_jenis_kelamin);
        foreach ($data_jenis_kelamin as $key => $jk) {
            $jenis_kelamin[] = $jk->jenis_kelamin;
        }

        $data_total_pinjaman = $this->M_Datatrain->get_total_pinjaman();
        $count_total_pinjaman = count($data_total_pinjaman);
        foreach ($data_total_pinjaman as $key => $tp) {
            $total_pinjaman[] = $tp->total_pinjaman;
        }

        $data_jumlah_tanggungan = $this->M_Datatrain->get_jumlah_tanggungan();
        $count_jumlah_tanggungan = count($data_jumlah_tanggungan);
        foreach ($data_jumlah_tanggungan as $key => $jt) {
            $jumlah_tanggungan[] = $jt->jumlah_tanggungan;
        }

        $data_status_pernikahan = $this->M_Datatrain->get_status_pernikahan();
        $count_status_pernikahan = count($data_status_pernikahan);
        foreach ($data_status_pernikahan as $key => $sp) {
            $status_pernikahan[] = $sp->status_pernikahan;
        }

        $data_pendapatan_utama = $this->M_Datatrain->get_pendapatan_utama();
        $count_pendapatan_utama = count($data_pendapatan_utama);
        foreach ($data_pendapatan_utama as $key => $dp) {
            $pendapatan_utama[] = $dp->pendapatan_utama;
        }

        $data_total_biaya_hidup = $this->M_Datatrain->get_total_biaya_hidup();
        $count_total_biaya_hidup = count($data_total_biaya_hidup);
        foreach ($data_total_biaya_hidup as $key => $tbh) {
            $total_biaya_hidup[] = $tbh->total_biaya_hidup;
        }

        $data_status_pinjaman = $this->M_Datatrain->get_status_pinjaman();
        $count_status_pinjaman = count($data_status_pinjaman);
        foreach ($data_status_pinjaman as $key => $spj) {
            $status_pinjaman[] = $spj->status_pinjaman;
        }

        for ($i=1; $i <= 30; $i++) { 
            $jk_rand = $jenis_kelamin[rand(0, $count_jenis_kelamin-1)];
            $tp_rand = $total_pinjaman[rand(0,$count_total_pinjaman-1)];
            $jt_rand = $jumlah_tanggungan[rand(0,$count_jumlah_tanggungan-1)];
            $sp_rand = $status_pernikahan[rand(0,$count_status_pernikahan-1)];
            $pu_rand = $pendapatan_utama[rand(0,$count_pendapatan_utama-1)];
            $tbh_rand = $total_biaya_hidup[rand(0,$count_total_biaya_hidup-1)];
            $spj_rand = $status_pinjaman[rand(0,$count_status_pinjaman-1)];
            $data[] = array('id' => $i, 'jenis_kelamin' => $jk_rand, 'total_pinjaman' => $tp_rand, 'jumlah_tanggungan' => $jt_rand, 'status_pernikahan' => $sp_rand, 'pendapatan_utama' => $pu_rand, 'total_biaya_hidup' => $tbh_rand, 'status_pinjaman' => $spj_rand);
		}
		$this->M_Datatest->insert_batch($data);
    }

	public function convert_datatrain_to_arff()
    {
		if (file_exists("./assets/uploads/datatrain.arff")) {
			unlink("./assets/uploads/datatrain.arff");
		}

		$cmd = "java -cp C:\/xampp\htdocs\KSP-MitraRaya\assets\weka.jar weka.core.converters.CSVLoader C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.csv > C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.arff";
		exec($cmd, $output);
	}
	
	public function create_model()
	{
		if (file_exists("./assets/uploads/datatrain.model")) {
			unlink("./assets/uploads/datatrain.model");
		}
		$cmd = shell_exec("java -cp C:\/xampp\htdocs\KSP-MitraRaya\assets\weka.jar weka.classifiers.trees.J48 -t C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.arff -d C:\/xampp\htdocs\KSP-MitraRaya\assets\uploads\datatrain.model");
	}
}
