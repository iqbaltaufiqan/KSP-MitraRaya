<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url('auth'));
		}
		$this->load->model('M_Pengajuan');
	}

	public function index()
	{
		$data['num_rows'] = $this->M_Pengajuan->get_num_rows();
		$data['num_rows_lancar'] = $this->M_Pengajuan->get_num_rows_lancar();
		$data['num_rows_macet'] = $this->M_Pengajuan->get_num_rows_macet();
		$this->load->view('dashboard/dashboard.php', $data);
	}

	public function admin()
	{
		$data['num_rows'] = $this->M_Pengajuan->get_num_rows();
		$data['num_rows_lancar'] = $this->M_Pengajuan->get_num_rows_lancar();
		$data['num_rows_macet'] = $this->M_Pengajuan->get_num_rows_macet();
		$this->load->view('dashboard/dashboard_admin.php', $data);
	}

	public function get_values_to_pie_chart()
	{
		header('Content-Type: application/json');
		$data = $this->M_Pengajuan->get_data_to_build_pie_chart();
		$data_json = array();
		foreach ($data as $key => $value) {
			$data_json[] = $value;
		}
		echo json_encode($data_json);
	}

	public function get_values_to_bar_chart()
	{
		header('Content-Type: application/json');
		$data = $this->M_Pengajuan->get_data_to_build_bar_chart();
		$data_json = array();
		foreach ($data as $key => $value) {
			$data_json[] = $value;
		}
		echo json_encode($data_json);
	}
}
