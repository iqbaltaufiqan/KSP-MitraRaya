<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nasabah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url('auth'));
		}
		$this->load->model('M_Nasabah');
	}

	public function index()
	{
		$data['nasabah'] = $this->M_Nasabah->get_data();
		$this->load->view('Nasabah/data_nasabah.php', $data);
	}

	public function admin(Type $var = null)
	{
		$data['nasabah'] = $this->M_Nasabah->get_data();
		$this->load->view('Nasabah/data_nasabah_admin.php', $data);
	}
}
