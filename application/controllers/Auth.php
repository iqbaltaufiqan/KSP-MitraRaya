<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_User');
	}

	public function index()
	{
		$this->load->view('login.php');
    }
    
    public function proses_login()
    {
        $username = $this->input->post('username');
		$pwd = $this->input->post('password');

		$data_login = array(
			'username' => $username,
			'password' => $pwd
		);
			
		$cek = $this->M_User->get_data_num_rows($username, $pwd);
		if ($cek > 0) {
				
			$cek_res = $this->M_User->get_data_by_cond($username, $pwd);

			$sess_data['id'] = NULL;
			$sess_data['username'] = NULL;
            $sess_data['is_pimpinan'] = NULL;
            $sess_data['foto'] = NULL;
			$sess_data['status'] = NULL;
			foreach ($cek_res as $data) {
				$sess_data['id'] = $data->id;
				$sess_data['username'] = $data->username;
                $sess_data['is_pimpinan'] = $data->is_pimpinan;
                $sess_data['foto'] = $data->foto;
				$sess_data['status'] = "login";
			}
			$this->session->set_userdata($sess_data);

			if ($sess_data['is_pimpinan'] == 1) {
					redirect(base_url('dashboard'));
			}
			else{
				redirect(base_url('dashboard/admin'));
			}
		}
		else{
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" style=\"pointer:cursor;position: fixed; z-index: 9999; top: 0px;margin-top: 50px; margin-left: 22px; width:97%;\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a> Login Gagal!!!</div>");
			redirect(base_url('auth'));
		}
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
        exit();
    }
}
