        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <form class="sidebar-search  " action="#" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                        <li class="nav-item start <?php if ($this->uri->segment(1)=="dashboard") {echo "active";}?>">
                            <a href="<?php echo base_url('dashboard/admin')?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item start <?php if ($this->uri->segment(2)=="lihat_akurasi") {echo "active";}?>">
                            <a href="<?php echo base_url('prediksinasabah/lihat_akurasi')?>" class="nav-link nav-toggle">
                                <i class="fa fa-tachometer"></i>
                                <span class="title">Lihat Akurasi</span>
                            </a>
                        </li>
                        <li class="nav-item start <?php if ($this->uri->segment(2)=="lihat_tree") {echo "active";}?>">
                            <a href="<?php echo base_url('prediksinasabah/lihat_tree')?>" class="nav-link nav-toggle">
                                <i class="fa fa-tree"></i>
                                <span class="title">Lihat Tree</span>
                            </a>
                        </li>
                        <li class="nav-item  <?php if ($this->uri->segment(1)=="dataset") {echo "active";}?>">
                            <a href="<?php echo base_url('dataset/admin');?>" class="nav-link nav-toggle">
                                <i class="icon-layers"></i>
                                <span class="title">Input Dataset</span>
                            </a>
                        </li>
                        <li class="nav-item  <?php if ($this->uri->segment(1)=="nasabah") {echo "active";}?>">
                            <a href="<?php echo base_url('nasabah/admin');?>" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Data Nasabah</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
