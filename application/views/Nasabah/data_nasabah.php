<?php
$this->load->view('templates/head.php');
$this->load->view('templates/topbar.php');
$this->load->view('templates/sidebar.php');
?>            
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="<?php echo base_url('dashboard')?>">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    Data Nasabah
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <!-- <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div> -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        
                        <!-- END PAGE HEADER-->
                        <div class="note note-success" style="margin-top:15px;">
                            <h4>Menu Data Nasabah dapat digunakan untuk melihat data pengajuan nasabah beserta dengan preferensinya</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit portlet-form bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-users font-green"></i>
                                            <span class="caption-subject font-green sbold uppercase"> Data Nasabah</span>
                                        </div>
                                        <div class="tools">
                                            <!-- <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="fullscreen"> </a>
                                            <a href="javascript:;" class="remove"> </a> -->
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div style="margin-left:10px; margin-right:10px;">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Jenis Kelamin</th>
                                                    <th>Alamat</th>
                                                    <th>Pekerjaan</th>
                                                    <th>Status Pernikahan</th>
                                                    <th>Jumlah Tanggungan</th>
                                                    <th>Total Pinjaman</th>
                                                    <th>Tipe Agunan</th>
                                                    <th>Pendapatan Utama</th>
                                                    <th>Total Biaya Hidup</th>
                                                    <th>Status Pinjaman</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($nasabah as $key => $n) {
                                                
                                                ?>
                                                <tr>
                                                    <th><?php echo $key+1;?></th>
                                                    <th><?php echo $n->nama;?></th>
                                                    <th><?php echo $n->jenis_kelamin;?></th>
                                                    <th><?php echo $n->alamat;?></th>
                                                    <th><?php echo $n->pekerjaan;?></th>
                                                    <th><?php echo $n->status_pernikahan;?></th>
                                                    <th><?php echo $n->jumlah_tanggungan;?></th>
                                                    <th><?php echo $n->total_pinjaman;?></th>
                                                    <th><?php echo $n->agunan;?></th>
                                                    <th><?php echo $n->pendapatan_utama;?></th>
                                                    <th><?php echo $n->total_biaya_hidup;?></th>
                                                    <th><?php echo $n->status_pinjaman;?></th>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->    
<?php
$this->load->view('templates/footer.php');
?>