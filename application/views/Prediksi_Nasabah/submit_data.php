<?php
$this->load->view('templates/head.php');
$this->load->view('templates/topbar.php');
$this->load->view('templates/sidebar.php');
?>            
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="<?php echo base_url('dashboard')?>">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Prediksi Nasabah</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    Submit Data
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <!-- <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div> -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        
                        <!-- END PAGE HEADER-->
                        <div class="note note-success" style="margin-top:15px;">
                            <h4>Menu Submit Data dapat digunakan untuk memprediksi pengajuan pinjaman baru berdasarkan parameter Jenis Kelamin, Total Pinjaman, Jumlah Tanggungan, Status Perkawinan, Pendapatan Utama, dan Total Biaya Hidup</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit portlet-form bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-layers font-green"></i>
                                            <span class="caption-subject font-green sbold uppercase"> Form Submit</span>
                                        </div>
                                        <div class="tools">
                                            <!-- <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="fullscreen"> </a>
                                            <a href="javascript:;" class="remove"> </a> -->
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <form action="<?php echo base_url('PrediksiNasabah/proses_submit');?>" class="form-horizontal"  method="POST">
                                            <div class="form-body">
                                                <div class="alert alert-danger display-hide">
                                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                                </div>
                                                <!-- <div class="alert alert-success display-hide">
                                                    <button class="close" data-close="alert"></button> Your form validation is successful!
                                                </div> -->
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Nama
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="" name="nama" required>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Isikan nama lengkap</span>
                                                    </div>
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Jenis Kelamin
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="jenis_kelamin" required>
                                                            <option value="">Select</option>
                                                            <option value="L">Laki-laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select>
                                                        <div class="form-control-focus"> </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Total Pinjaman
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="" name="total_pinjaman" required>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Isikan total pinjaman</span>
                                                    </div>
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Jumlah Tanggungan
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="" name="jumlah_tanggungan" required>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Isikan jumlah tanggungan</span>
                                                    </div>
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Status Perkawinan
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="status_perkawinan" required>
                                                            <option value="">Select</option>
                                                            <option value="MENIKAH">Menikah</option>
                                                            <option value="BELUM_MENIKAH">Belum Menikah</option>
                                                            <option value="CERAI">Cerai</option>
                                                        </select>
                                                        <div class="form-control-focus"> </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Pendapatan Utama
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="" name="pendapatan_utama" required>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Isikan pendapatan utama</span>
                                                    </div>
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-3 control-label" for="form_control_1">Total Biaya Hidup
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="" name="total_biaya_hidup" required>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Isikan total biaya hidup</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button class="btn green">Submit</button>
                                                        <button type="reset" class="btn default">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->    
<?php
$this->load->view('templates/footer.php');
?>