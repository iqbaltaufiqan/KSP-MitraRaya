<?php
$this->load->view('templates/head.php');
$this->load->view('templates/topbar.php');
$this->load->view('templates/sidebar.php');
?>
<style>
    .table>tbody>tr>td{
        border-top: 0 !important;
    }
</style>            
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="<?php echo base_url('dashboard')?>">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Prediksi Nasabah</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Submit Data</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    Hasil Prediksi
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <!-- <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div> -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        
                        <!-- END PAGE HEADER-->
                        <!-- <div class="note note-success" style="margin-top:15px;">
                            <h4>Menu Lihat Akurasi dapat digunakan untuk melihat tingkat akurasi yang dihasilkan dari sistem</h4>
                        </div> -->
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit portlet-form bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-layers font-green"></i>
                                            <span class="caption-subject font-green sbold uppercase"> Hasil Prediksi</span>
                                        </div>
                                        <div class="tools">
                                            <!-- <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="fullscreen"> </a>
                                            <a href="javascript:;" class="remove"> </a> -->
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div style="margin-left:20px; margin-right:20px;">
                                            <table class="table">
                                                <?php foreach ($pengajuan as $key => $value) {
                                                ?>
                                                <tbody>
                                                    <tr>
                                                        <td>Nama:</td>
                                                        <td><?php echo $value->nama;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Kelamin:</td>
                                                        <td><?php echo $value->jenis_kelamin;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Pinjaman:</td>
                                                        <td><?php echo $value->total_pinjaman;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah Tanggungan:</td>
                                                        <td><?php echo $value->jumlah_tanggungan;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status Pernikahan:</td>
                                                        <td><?php echo $value->status_pernikahan;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pendapatan Utama:</td>
                                                        <td><?php echo $value->pendapatan_utama;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Biaya Hidup:</td>
                                                        <td><?php echo $value->total_biaya_hidup;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hasil Prediksi:</td>
                                                        <td>
                                                            <?php $value->status_pinjaman;
                                                                if ($value->status_pinjaman == "MACET") {
                                                                    echo "<span class=\"label label-md label-danger\">".$value->status_pinjaman."</span>";
                                                                }
                                                                else{
                                                                    echo "<span class=\"label label-md label-success\">".$value->status_pinjaman."</span>";
                                                                }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->    
<?php
$this->load->view('templates/footer.php');
?>