<?php
$this->load->view('templates/head.php');
$this->load->view('templates/topbar.php');
$this->load->view('templates/sidebar2.php');
?>            
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="<?php echo base_url('dashboard')?>">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    Lihat Tree
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <!-- <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div> -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        
                        <!-- END PAGE HEADER-->
                        <div class="note note-success" style="margin-top:15px;">
                            <h4>Menu Lihat Tree dapat digunakan untuk melihat rule yang dihasilkan berupa tree dari sistem</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit portlet-form bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-layers font-green"></i>
                                            <span class="caption-subject font-green sbold uppercase"> Pohon Keputusan</span>
                                        </div>
                                        <div class="tools">
                                            <!-- <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="fullscreen"> </a>
                                            <a href="javascript:;" class="remove"> </a> -->
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div style="margin-left: 20px; margin-right: 20px;">
                                            <?php 
                                                echo "<pre>".$row."</pre>";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->    
<?php
$this->load->view('templates/footer.php');
?>