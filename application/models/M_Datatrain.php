<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class M_Datatrain extends CI_Model
	{

		public function input($data)
		{
			$this->db->insert_batch('datatrain', $data);
		}

		public function get_num_rows_data()
		{
			$query = $this->db->query('SELECT * FROM datatrain');
			return $query->num_rows();
		}

		public function get_jenis_kelamin()
		{
			$query = $this->db->query('SELECT DISTINCT jenis_kelamin FROM datatrain');
			return $query->result();
		}

		public function get_total_pinjaman()
		{
			$query = $this->db->query('SELECT DISTINCT total_pinjaman FROM datatrain');
			return $query->result();
		}

		public function get_jumlah_tanggungan()
		{
			$query = $this->db->query('SELECT DISTINCT jumlah_tanggungan FROM datatrain');
			return $query->result();
		}

		public function get_status_pernikahan()
		{
			$query = $this->db->query('SELECT DISTINCT status_pernikahan FROM datatrain');
			return $query->result();
		}

		public function get_pendapatan_utama()
		{
			$query = $this->db->query('SELECT DISTINCT pendapatan_utama FROM datatrain');
			return $query->result();
		}

		public function get_total_biaya_hidup()
		{
			$query = $this->db->query('SELECT DISTINCT total_biaya_hidup FROM datatrain');
			return $query->result();
		}

		public function get_status_pinjaman()
		{
			$query = $this->db->query('SELECT DISTINCT status_pinjaman FROM datatrain');
			return $query->result();
		}

		public function delete_all_data()
		{
			$query = $this->db->query('DELETE FROM datatrain');
		}
	}
?>