<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class M_Nasabah extends CI_Model
	{

		public function input($data)
		{
			$this->db->insert_batch('nasabah', $data);
		}

		public function get_row()
		{
			$query = $this->db->query('SELECT * FROM nasabah n JOIN transaksi t on n.id = t.id_nasabah');
			return $query->num_rows();
		}

		public function get_data()
		{
			$query = $this->db->query('SELECT * FROM nasabah n JOIN transaksi t on n.id = t.id_nasabah ORDER BY n.nama');
			return $query->result();
		}

		public function delete_all_data()
		{
			$query = $this->db->query('DELETE FROM nasabah');
		}

		public function get_records_to_create_csv()
		{
			$query = $this->db->query('SELECT n.jenis_kelamin, t.total_pinjaman, n.jumlah_tanggungan, n.status_pernikahan, t.pendapatan_utama, t.total_biaya_hidup, t.status_pinjaman FROM nasabah n JOIN transaksi t on n.id = t.id_nasabah WHERE n.jenis_kelamin != "?" AND t.total_pinjaman != "?" AND n.jumlah_tanggungan != "?" AND n.status_pernikahan != "?" AND t.pendapatan_utama != "?" AND t.total_biaya_hidup != "?" AND t.status_pinjaman != "?"');
			return $query->result();
		}

		public function delete($cond, $table)
		{
			$this->db->where($cond);
			$this->db->delete($table);
		}
	}
?>