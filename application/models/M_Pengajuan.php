<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class M_Pengajuan extends CI_Model
	{

		public function input($data)
		{
			$this->db->insert('pengajuan', $data);
        }
        
        public function get_data()
        {
            $query = $this->db->query('SELECT * FROM pengajuan');
			return $query->result();
		}
		
		public function get_num_rows()
        {
            $query = $this->db->query('SELECT * FROM pengajuan');
			return $query->num_rows();
		}

		public function get_num_rows_lancar()
		{
			$query = $this->db->query('SELECT * FROM pengajuan WHERE status_pinjaman = "LANCAR"');
			return $query->num_rows();
		}
		
		public function get_num_rows_macet()
		{
			$query = $this->db->query('SELECT * FROM pengajuan WHERE status_pinjaman = "MACET"');
			return $query->num_rows();
		}

		public function get_data_to_build_line_chart()
		{
			$query = $this->db->query('SELECT CONCAT(MONTH(tgl_pengajuan),"-",YEAR(tgl_pengajuan)) AS tgl_pengajuan, COUNT(CASE status_pinjaman WHEN "LANCAR" then 1 else null end) AS diterima, COUNT(case status_pinjaman when "MACET" then 1 else null end) AS ditolak from pengajuan group by concat(MONTH(tgl_pengajuan),"-",YEAR(tgl_pengajuan)) order by YEAR(tgl_pengajuan), MONTH(tgl_pengajuan)');
			return $query->result();
		}

		public function get_data_to_build_pie_chart()
		{
			$query = $this->db->query('SELECT status_pinjaman, count(status_pinjaman) as value from pengajuan group by status_pinjaman');
			return $query->result();
		}

		public function get_data_to_build_bar_chart()
		{
			$query = $this->db->query('SELECT MONTHNAME(tgl_pengajuan) AS tgl_pengajuan, COUNT(CASE status_pinjaman WHEN "LANCAR" then 1 else null end) AS diterima, COUNT(case status_pinjaman when "MACET" then 1 else null end) AS ditolak from pengajuan group by concat(MONTH(tgl_pengajuan),"-",YEAR(tgl_pengajuan)) order by YEAR(tgl_pengajuan), MONTH(tgl_pengajuan)');
			return $query->result();
		}

		public function get_last_record()
		{
			$query = $this->db->query('SELECT * FROM pengajuan ORDER BY id DESC LIMIT 1');
			return $query->result();
        }
        
        public function update($data, $cond)
		{
			$this->db->where($cond);
			$this->db->update('pengajuan', $data);
		}
	}
?>