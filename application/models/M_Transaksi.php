<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class M_Transaksi extends CI_Model
	{

		public function input($data)
		{
			$this->db->insert_batch('transaksi', $data);
		}

		public function delete_all_data()
		{
			$query = $this->db->query('DELETE FROM transaksi');
		}
	}
?>