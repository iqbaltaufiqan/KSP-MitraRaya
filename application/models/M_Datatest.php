<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class M_Datatest extends CI_Model
	{

		public function input($data)
		{
			$this->db->insert('datatest', $data);
		}

		public function insert_batch($data)
		{
			$this->db->insert_batch('datatest', $data);
		}

		public function get_last_record()
		{
			$query = $this->db->query('SELECT * FROM datatest ORDER BY id DESC LIMIT 1');
			return $query->result();
		}
		
		public function get_data_desc()
		{
			$query = $this->db->query('SELECT * FROM datatest ORDER BY id DESC');
			return $query->result();
		}

		public function get_num_rows_data()
		{
			$query = $this->db->query('SELECT * FROM datatest');
			return $query->num_rows();
		}

		public function delete_all_data()
		{
			$query = $this->db->query('DELETE FROM datatest');
		}
	}
?>