var ChartsAmcharts = function() {
    $.ajax({
        url: 'dashboard/get_values_to_pie_chart',
        method: "GET",
        async: true,
        success: function (data) {
            var chartData = [];
            for (var i in data) {
                var status;
                if (data[i].status_pinjaman == "MACET") {
                    status = "DITOLAK";
                }
                else{
                    status = "DITERIMA";
                }
                chartData.push({
                    status_pinjaman:status,
                    value:data[i].value
                });
            }
            var chart = AmCharts.makeChart("chart_1", {
                "type": "pie",
                "theme": "light",
                "dataProvider": chartData,
                "valueField": "value",
                "titleField": "status_pinjaman",
                "balloon":{
                    "fixedPosition":true
                },
                "export": {
                    "enabled": true
                }
            });
    
            $('#chart_1').closest('.portlet').find('.fullscreen').click(function() {
                chart.invalidateSize();
            });
        }
    });
    $.ajax({
        url: 'dashboard/get_values_to_bar_chart',
        method: "GET",
        async: true,
        success: function (data) {
            var chartData = [];
            for (var i in data) {
                chartData.push({
                    tgl:data[i].tgl_pengajuan,
                    diterima:data[i].diterima,
                    ditolak:data[i].ditolak
                });
            }
            var chart = AmCharts.makeChart("chart_2", {
                "type": "serial",
                "theme": "light",
                "categoryField": "tgl",
                "dataDateFormat": "MM",
                "rotate": false,
                "startDuration": 1,
                "categoryAxis": {
                    "gridPosition": "start",
                    "position": "left",
                    "title": "Bulan"
                },
                "trendLines": [],
                "graphs": [
                    {
                        "balloonText": "Diterima:[[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-1",
                        "lineAlpha": 0.2,
                        "title": "Diterima",
                        "type": "column",
                        "valueField": "diterima"
                    },
                    {
                        "balloonText": "Ditolak:[[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-2",
                        "lineAlpha": 0.2,
                        "title": "Ditolak",
                        "type": "column",
                        "valueField": "ditolak"
                    }
                ],
                "guides": [],
                "valueAxes": [
                    {
                        "id": "ValueAxis-1",
                        "position": "bottom",
                        "axisAlpha": 0,
                        "title": "Total Pengajuan"
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "titles": [],
                "dataProvider": chartData,
                "export": {
                    "enabled": true
                }
            });
    
            $('#chart_2').closest('.portlet').find('.fullscreen').click(function() {
                chart.invalidateSize();
            });
        }
    });
    $.ajax({
        url: 'dashboard/get_values_to_bar_chart',
        method: "GET",
        async: true,
        success: function (data) {
            var chartData = [];
            for (var i in data) {
                chartData.push({
                    tgl:data[i].tgl_pengajuan,
                    diterima:data[i].diterima,
                    ditolak:data[i].ditolak
                });
            }
            var chart = AmCharts.makeChart("chart_3", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": chartData,
                "valueAxes": [{
                    "integersOnly": true,
                    "axisAlpha": 0,
                    "dashLength": 5,
                    "gridCount": 10,
                    "position": "left",
                    "title": "Total Pengajuan"
                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "Jumlah pengajuan ditolak [[category]]: [[value]]",
                    "bullet": "round",
                    "title": "Ditolak",
                    "valueField": "ditolak",
                    "fillAlphas": 0
                }, {
                    "balloonText": "Jumlah pengajuan diterima [[category]]: [[value]]",
                    "bullet": "round",
                    "title": "Diterima",
                    "valueField": "diterima",
                    "fillAlphas": 0
                }],
                "chartCursor": {
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "tgl",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "fillAlpha": 0.05,
                    "fillColor": "#000000",
                    "gridAlpha": 0,
                    "position": "bottom",
                    "title": "Bulan"
                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });
    
            $('#chart_3').closest('.portlet').find('.fullscreen').click(function() {
                chart.invalidateSize();
            });
        }
    });
    $.ajax({
        url: 'get_values_to_pie_chart',
        method: "GET",
        async: true,
        success: function (data) {
            var chartData = [];
            for (var i in data) {
                var status;
                if (data[i].status_pinjaman == "MACET") {
                    status = "DITOLAK";
                }
                else{
                    status = "DITERIMA";
                }
                chartData.push({
                    status_pinjaman:status,
                    value:data[i].value
                });
            }
            var chart = AmCharts.makeChart("chart_4", {
                "type": "pie",
                "theme": "light",
                "dataProvider": chartData,
                "valueField": "value",
                "titleField": "status_pinjaman",
                "balloon":{
                    "fixedPosition":true
                },
                "export": {
                    "enabled": true
                }
            });
    
            $('#chart_4').closest('.portlet').find('.fullscreen').click(function() {
                chart.invalidateSize();
            });
        }
    });
    $.ajax({
        url: 'get_values_to_bar_chart',
        method: "GET",
        async: true,
        success: function (data) {
            var chartData = [];
            for (var i in data) {
                chartData.push({
                    tgl:data[i].tgl_pengajuan,
                    diterima:data[i].diterima,
                    ditolak:data[i].ditolak
                });
            }
            var chart = AmCharts.makeChart("chart_5", {
                "type": "serial",
                "theme": "light",
                "categoryField": "tgl",
                "dataDateFormat": "MM",
                "rotate": false,
                "startDuration": 1,
                "categoryAxis": {
                    "gridPosition": "start",
                    "position": "left",
                    "title": "Bulan"
                },
                "trendLines": [],
                "graphs": [
                    {
                        "balloonText": "Diterima:[[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-1",
                        "lineAlpha": 0.2,
                        "title": "Diterima",
                        "type": "column",
                        "valueField": "diterima"
                    },
                    {
                        "balloonText": "Ditolak:[[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-2",
                        "lineAlpha": 0.2,
                        "title": "Ditolak",
                        "type": "column",
                        "valueField": "ditolak"
                    }
                ],
                "guides": [],
                "valueAxes": [
                    {
                        "id": "ValueAxis-1",
                        "position": "bottom",
                        "axisAlpha": 0,
                        "title": "Total Pengajuan"
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "titles": [],
                "dataProvider": chartData,
                "export": {
                    "enabled": true
                }
            });
    
            $('#chart_5').closest('.portlet').find('.fullscreen').click(function() {
                chart.invalidateSize();
            });
        }
    });
    $.ajax({
        url: 'get_values_to_bar_chart',
        method: "GET",
        async: true,
        success: function (data) {
            var chartData = [];
            for (var i in data) {
                chartData.push({
                    tgl:data[i].tgl_pengajuan,
                    diterima:data[i].diterima,
                    ditolak:data[i].ditolak
                });
            }
            var chart = AmCharts.makeChart("chart_6", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": chartData,
                "valueAxes": [{
                    "integersOnly": true,
                    "axisAlpha": 0,
                    "dashLength": 5,
                    "gridCount": 10,
                    "position": "left",
                    "title": "Total Pengajuan"
                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "Jumlah pengajuan ditolak [[category]]: [[value]]",
                    "bullet": "round",
                    "title": "Ditolak",
                    "valueField": "ditolak",
                    "fillAlphas": 0
                }, {
                    "balloonText": "Jumlah pengajuan diterima [[category]]: [[value]]",
                    "bullet": "round",
                    "title": "Diterima",
                    "valueField": "diterima",
                    "fillAlphas": 0
                }],
                "chartCursor": {
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "tgl",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "fillAlpha": 0.05,
                    "fillColor": "#000000",
                    "gridAlpha": 0,
                    "position": "bottom",
                    "title": "Bulan"
                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });
    
            $('#chart_6').closest('.portlet').find('.fullscreen').click(function() {
                chart.invalidateSize();
            });
        }
    });
    var initChartSample1 = function() {

    }

    return {
        //main function to initiate the module

        init: function() {

            initChartSample1();
        }

    };

}();

jQuery(document).ready(function() {    
   ChartsAmcharts.init(); 
});